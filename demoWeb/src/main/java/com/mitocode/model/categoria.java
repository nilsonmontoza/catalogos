package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class categoria {
	@Id
	private int idCategoria;
	
	@Column(name = "nombre", length = 50)
	private String nombre;
	
	@Column(name = "tipo")
	private int tipo;
	
	@Column(name = "foto")
	private int foto;
	
	@Column(name = "categoriaPadre")
	private int categoriaPadre;
	
	public int getidCategoria() {
		return idCategoria;
	}
	public void setidCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getFoto() {
		return foto;
	}
	public void setFoto(int foto) {
		this.foto = foto;
	}
	public int getCategoriaPadre() {
		return categoriaPadre;
	}
	public void setCategoriaPadre(int categoriaPadre) {
		this.categoriaPadre = categoriaPadre;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
}
