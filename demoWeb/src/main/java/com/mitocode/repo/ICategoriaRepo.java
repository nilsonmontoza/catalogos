package com.mitocode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.categoria;

public interface ICategoriaRepo extends JpaRepository<categoria, Integer>{

}
