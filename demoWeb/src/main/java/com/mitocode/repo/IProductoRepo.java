package com.mitocode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.producto;

public interface IProductoRepo extends JpaRepository<producto, Integer>{

}
