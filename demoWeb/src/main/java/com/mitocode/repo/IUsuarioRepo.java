package com.mitocode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.usuario;

public interface IUsuarioRepo extends JpaRepository<usuario, Integer>{

}
