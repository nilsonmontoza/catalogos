package com.mitocode.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.categoria;
import com.mitocode.repo.ICategoriaRepo;

@RestController
@RequestMapping("/categoria")
public class RestCategoriaController {
	
	@Autowired
	private ICategoriaRepo repo;
	
	@GetMapping
	public List<categoria> listar(){
		//funcion para listar
		return repo.findAll();
	}
	
	@GetMapping(value = "/{id}")
	public Optional<categoria> listar_sub(@PathVariable Integer id){
		//funcion para listar
		return repo.findById(id);
	}
	
	@PostMapping
	public void insert(@RequestBody categoria cate){
		//funcion insertar
		repo.save(cate);
	}
}
