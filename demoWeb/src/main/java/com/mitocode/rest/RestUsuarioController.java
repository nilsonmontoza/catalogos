package com.mitocode.rest;

import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.usuario;
import com.mitocode.repo.IUsuarioRepo;

@RestController
@RequestMapping("/usuario")
public class RestUsuarioController {

	@Autowired
	private IUsuarioRepo repo;
	
	@GetMapping
	public List<usuario> listar(){
		//funcion para listar
		String query = "SELECT * FROM categoria LIMIT 3";
		return repo.findAll();
	}
	
	@PostMapping
	public void insert(@RequestBody usuario user){
		repo.save(user);
	}
}
