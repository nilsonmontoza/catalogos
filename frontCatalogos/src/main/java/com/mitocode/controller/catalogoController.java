package com.mitocode.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
public class catalogoController {

	
	@GetMapping("/home")
	public String home(Model model) throws Exception {
		
		RestTemplate plantilla = new RestTemplate();
	    List<?> resultado = plantilla.getForObject("http://localhost:8080/categoria", List.class);
		System.out.print(resultado);
		model.addAttribute("categorias", resultado);
		return "home";
	}
	
	@GetMapping("/categoria")
	public String sub_categoria(@RequestParam(name = "name") String name, Model model) throws Exception {
		RestTemplate plantilla = new RestTemplate();
		Object resultado = plantilla.getForObject("http://localhost:8080/categoria/"+name, Object.class);
		System.out.print(resultado);
		model.addAttribute("sub", resultado);
		
		List<?> resultado2 = plantilla.getForObject("http://localhost:8080/producto", List.class);
		
		model.addAttribute("pro", resultado2);
		
		return "categoria";
	}

	 //@GetMapping("/home")
		//public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) throws Exception {
			//model.addAttribute("name", name);
			//run();
			//return "home";
		//}
}
